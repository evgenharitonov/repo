﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;


public class SQLManagment : MonoBehaviour
{

	public GameObject buttonPrefab, buttonPrefab2, buttonPrefab3, namepanel, colorpanel, mainpanel, tableselect;
	private GameObject tempGameObj;
	public InputField nameInput;
	public Dropdown dropdown;
	public Slider LoadingBar;
	public string[] nameOfColumn, items;
	string columnname, tempColumnName, tempColumnData, tempText;
	int tInd, tempIndex, tempDropdownValue;
	string nameInList, nameInList2;
	public string WWWLoadManager, WWWSaveManager;
	string filePath;
	string fileName;
	public string tablename;

	public void Start()
	{
		StartCoroutine(ColumnsSpawn());
		StartCoroutine(ProgressBar(LoadingBar));
	}



	IEnumerator ColumnsSpawn()

	{
		WWWForm table = new WWWForm();
		table.AddField("tablename", tablename);
		WWW dataDB = new WWW(WWWLoadManager, table);
		yield return dataDB;
		string msg = dataDB.text;
		items = msg.Split('@');


		for (int b = 0; b < 1; b++)
		{
			for (int i = 0; i < items.Length - 1f; i++)
			{
				int tempI = i;
				string tempName = $"t{b + 1}";
				GameObject spwnButton = Instantiate(buttonPrefab, transform.position, Quaternion.identity) as GameObject;
				spwnButton.transform.SetParent(GameObject.FindGameObjectWithTag("CP" + b).transform, false);
				spwnButton.GetComponentInChildren<Text>().text = GetDataValue(items[i], $"t{b + 1}:");
				spwnButton.GetComponent<Button>().onClick.AddListener(() => OnClick(tempI, tempName, spwnButton));
				GameObject spwnButton2 = Instantiate(buttonPrefab, transform.position, Quaternion.identity) as GameObject;
				spwnButton2.transform.SetParent(GameObject.FindGameObjectWithTag("CP" + b).transform, false);
				spwnButton2.GetComponentInChildren<Text>().text = "";
			}
		}


		for (int b = 1; b < 2; b++)
		{
			for (int i = 0; i < items.Length - 1f; i++)
			{
				int tempI = i;
				string tempName = $"t{b + 1}";
				GameObject spwnButton = Instantiate(buttonPrefab, transform.position, Quaternion.identity) as GameObject;
				spwnButton.transform.SetParent(GameObject.FindGameObjectWithTag("CP" + b).transform, false);
				spwnButton.GetComponentInChildren<Text>().text = GetDataValue(items[i], $"t{b + 1}:");
				spwnButton.GetComponent<Button>().onClick.AddListener(() => OnClick(tempI, tempName, spwnButton));
				GameObject spwnButton2 = Instantiate(buttonPrefab2, transform.position, Quaternion.identity) as GameObject;
				spwnButton2.transform.SetParent(GameObject.FindGameObjectWithTag("CP" + b).transform, false);
				spwnButton2.GetComponentInChildren<Text>().text = "ОПЛАТА";
			}
		}

		for (int b = 18; b < 19; b++)
		{

			for (int i = 0; i < 12; i++)
			{
				int tempI = i;
				string tempName = $"t{b + 1}";
				GameObject spwnButton = Instantiate(buttonPrefab, transform.position, Quaternion.identity) as GameObject;
				spwnButton.transform.SetParent(GameObject.FindGameObjectWithTag("Header").transform, false);
				spwnButton.GetComponentInChildren<Text>().text = GetDataValue(items[i], $"t{b + 1}:");
				spwnButton.GetComponent<Button>().onClick.AddListener(() => OnClick(tempI, tempName, spwnButton));

			}
		}


		for (int b = 14; b < 18; b++)
		{
			for (int i = 0; i < items.Length - 1f; i++)
			{
				int tempI = i;
				string tempName = $"t{b + 1}";
				GameObject spwnButton = Instantiate(buttonPrefab, transform.position, Quaternion.identity) as GameObject;
				spwnButton.transform.SetParent(GameObject.FindGameObjectWithTag("CP" + b).transform, false);
				spwnButton.GetComponentInChildren<Text>().text = (GetDataValue(items[i], $"t{b + 1}:"));
				spwnButton.GetComponent<Button>().onClick.AddListener(() => OnClick(tempI, tempName, spwnButton));
				GameObject spwnButton2 = Instantiate(buttonPrefab, transform.position, Quaternion.identity) as GameObject;
				spwnButton2.transform.SetParent(GameObject.FindGameObjectWithTag("CP" + b).transform, false);
				spwnButton2.GetComponentInChildren<Text>().text = "";

			}
		}

		for (int b = 2; b < 14; b++)
		{
			for (int i = 0; i < items.Length - 1f; i++)
			{
				int c = 18 + b;
				nameInList = (GetDataValue(items[i], $"t{b + 1}:"));
				nameInList2 = (GetDataValue(items[i], $"t{b + 19}:"));
				int tempI = i;
				string tempName = nameOfColumn[b];
				string tempName2 = nameOfColumn[c];
				GameObject spwnButton2 = Instantiate(buttonPrefab3, transform.position, Quaternion.identity) as GameObject;
				spwnButton2.transform.SetParent(GameObject.FindGameObjectWithTag("CP" + b).transform, false);
				if (nameInList2 == "0" || nameInList2 == "-")
				{
					ColorBlock cb = spwnButton2.GetComponent<Button>().colors;
					//cb.normalColor = Color.white;
					//spwnButton2.GetComponent<Button>().colors = cb;
					spwnButton2.GetComponentInChildren<Text>().text = "-";
				}
				else if (nameInList2 == "1" || nameInList2 == "+")
				{
					ColorBlock cb = spwnButton2.GetComponent<Button>().colors;
					//cb.normalColor = Color.yellow;
					//spwnButton2.GetComponent<Button>().colors = cb;
					spwnButton2.GetComponentInChildren<Text>().text = "+";
				}
				spwnButton2.GetComponent<Button>().onClick.AddListener(() => OnClick3(tempI, tempName2, spwnButton2));


				GameObject spwnButton = Instantiate(buttonPrefab2, transform.position, Quaternion.identity) as GameObject;
				spwnButton.transform.SetParent(GameObject.FindGameObjectWithTag("CP" + b).transform, false);
				if (nameInList == "0")
				{
					ColorBlock cb = spwnButton.GetComponent<Button>().colors;
					cb.normalColor = Color.red;
					spwnButton.GetComponent<Button>().colors = cb;
				}

				else if (nameInList == "1")
				{
					ColorBlock cb = spwnButton.GetComponent<Button>().colors;
					cb.normalColor = Color.green;
					spwnButton.GetComponent<Button>().colors = cb;
				}
				else if (nameInList == "2")
				{
					ColorBlock cb = spwnButton.GetComponent<Button>().colors;
					cb.normalColor = Color.grey;
					spwnButton.GetComponent<Button>().colors = cb;
				}
				else if (nameInList == "3")
				{
					ColorBlock cb = spwnButton.GetComponent<Button>().colors;
					cb.normalColor = new Color32(102, 51, 153, 255);
					spwnButton.GetComponent<Button>().colors = cb;
				}
				else if (nameInList == "4")
				{
					ColorBlock cb = spwnButton.GetComponent<Button>().colors;
					cb.normalColor = Color.black;
					spwnButton.GetComponent<Button>().colors = cb;
				}
				else if (nameInList == "5")
				{
					ColorBlock cb = spwnButton.GetComponent<Button>().colors;
					cb.normalColor = new Color32(255, 105, 180, 255);
					spwnButton.GetComponent<Button>().colors = cb;
				}
				else if (nameInList == "6")
				{
					ColorBlock cb = spwnButton.GetComponent<Button>().colors;
					cb.normalColor = new Color32(255, 255, 255, 255);
					spwnButton.GetComponent<Button>().colors = cb;
				}
				else if (nameInList == "7")
				{
					ColorBlock cb = spwnButton.GetComponent<Button>().colors;
					cb.normalColor = new Color32(9, 45, 253, 255);
					spwnButton.GetComponent<Button>().colors = cb;
				}
				spwnButton.GetComponent<Button>().onClick.AddListener(() => OnClick2(tempI, tempName, spwnButton));
			}

		}

	}

	string GetDataValue(string data, string index)
	{
		string value = data.Substring(data.IndexOf(index) + index.Length);
		if (value.Contains("|")) value = value.Remove(value.IndexOf("|"));
		return value;
	}

	public void OnClick(int buttonindex, string buttonname, GameObject spwn)
	{
		tempIndex = buttonindex;
		tempColumnName = buttonname;
		namepanel.SetActive(true);
		tempGameObj = spwn;

	}

	public void OnClick2(int buttonindex, string buttonname, GameObject spwn)

	{
		tempIndex = buttonindex;
		tempColumnName = buttonname;
		colorpanel.SetActive(true);
		tempGameObj = spwn;
	}

	public void OnClick3(int buttonindex, string buttonname, GameObject spwn)

	{
		tempIndex = buttonindex;
		tempColumnName = buttonname;
		tempText = spwn.GetComponentInChildren<Text>().text;
		if (tempText == "1" || tempText == "+")
		{
			ColorBlock cb = spwn.GetComponent<Button>().colors;
			//cb.normalColor = Color.white;
			//spwn.GetComponent<Button>().colors = cb;
			spwn.GetComponentInChildren<Text>().text = "-";
			tempColumnData = "-";
			StartCoroutine(SaveColorButtons());
		}
		else if (tempText == "0" || tempText == "-")
		{
			ColorBlock cb = spwn.GetComponent<Button>().colors;
			//cb.normalColor = Color.yellow;
			//spwn.GetComponent<Button>().colors = cb;
			spwn.GetComponentInChildren<Text>().text = "+";
			tempColumnData = "+";
			StartCoroutine(SaveColorButtons());
		}

	}

	public void CloseButton()
	{
		namepanel.SetActive(false);
		colorpanel.SetActive(false);
		tableselect.SetActive(false);
	}


	public void TableSelect()
	{
		tableselect.SetActive(true);
	}

	public void SaveButton()
	{
		StartCoroutine(SaveInfo());
	}

	public void SaveColButton()
	{
		StartCoroutine(SaveColInfo());
	}

	public void Quit()
	{
		Application.Quit();
	}

	IEnumerator SaveInfo()
	{
		tempColumnData = nameInput.text;
		WWWForm table = new WWWForm();
		table.AddField("tid", tempIndex + 1);
		table.AddField("tablename", tablename);
		table.AddField("colmnname", tempColumnName);
		table.AddField("colmndata", tempColumnData);
		WWW connect = new WWW(WWWSaveManager, table);
		yield return connect;
		tempGameObj.GetComponentInChildren<Text>().text = nameInput.text;
		namepanel.SetActive(false);
	}

	IEnumerator SaveColInfo()
	{
		tempDropdownValue = dropdown.value;
		WWWForm table = new WWWForm();
		table.AddField("tid", tempIndex + 1);
		table.AddField("tablename", tablename);
		table.AddField("colmnname", tempColumnName);
		table.AddField("colmndata", tempDropdownValue);
		WWW connect = new WWW(WWWSaveManager, table);
		yield return connect;
		if (dropdown.value == 0)
		{
			ColorBlock cb = tempGameObj.GetComponent<Button>().colors;
			cb.normalColor = Color.red;
			tempGameObj.GetComponent<Button>().colors = cb;
		}

		else if (dropdown.value == 1)
		{
			ColorBlock cb = tempGameObj.GetComponent<Button>().colors;
			cb.normalColor = Color.green;
			tempGameObj.GetComponent<Button>().colors = cb;
		}
		else if (dropdown.value == 2)
		{
			ColorBlock cb = tempGameObj.GetComponent<Button>().colors;
			cb.normalColor = Color.grey;
			tempGameObj.GetComponent<Button>().colors = cb;
		}
		else if (dropdown.value == 3)
		{
			ColorBlock cb = tempGameObj.GetComponent<Button>().colors;
			cb.normalColor = new Color32(102, 51, 153, 255);
			tempGameObj.GetComponent<Button>().colors = cb; ;
		}
		else if (dropdown.value == 4)
		{
			ColorBlock cb = tempGameObj.GetComponent<Button>().colors;
			cb.normalColor = Color.black;
			tempGameObj.GetComponent<Button>().colors = cb;
		}
		else if (dropdown.value == 5)
		{
			ColorBlock cb = tempGameObj.GetComponent<Button>().colors;
			cb.normalColor = new Color32(255, 105, 180, 255);
			tempGameObj.GetComponent<Button>().colors = cb;
		}
		else if (dropdown.value == 6)
		{
			ColorBlock cb = tempGameObj.GetComponent<Button>().colors;
			cb.normalColor = new Color32(255, 255, 255, 255);
			//206, 255, 243, 255
			tempGameObj.GetComponent<Button>().colors = cb;
		}

		else if (dropdown.value == 7)
		{
			ColorBlock cb = tempGameObj.GetComponent<Button>().colors;
			cb.normalColor = new Color32(9, 45, 253, 255);
			//206, 255, 243, 255
			tempGameObj.GetComponent<Button>().colors = cb;
		}
		namepanel.SetActive(false);
		colorpanel.SetActive(false);
	}

	IEnumerator SaveColorButtons()
	{
		WWWForm table = new WWWForm();
		table.AddField("tid", tempIndex + 1);
		table.AddField("tablename", tablename);
		table.AddField("colmnname", tempColumnName);
		table.AddField("colmndata", tempColumnData);
		WWW connect = new WWW(WWWSaveManager, table);
		yield return connect;
	}

IEnumerator ProgressBar(Slider slider)
{
	slider.minValue = 0;
	slider.maxValue = 100;
	slider.wholeNumbers = false;
	slider.value = 0;
	float timeSlice = (slider.maxValue / 3.1f);
	while (slider.value <= 100)
	{
		slider.value += timeSlice;
		yield return new WaitForSeconds(0.2f);
		if (slider.value >= 100)
			break;
	}
	mainpanel.SetActive(false);
}
}