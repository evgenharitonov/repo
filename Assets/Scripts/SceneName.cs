﻿using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;

public class SceneName : MonoBehaviour
{
	public TMP_Text header;

	void Start()
	{
		int headername = SceneManager.GetActiveScene().buildIndex;
		switch (headername)
		{
			case 0:
				header.text = "Сакко";
				break;
			case 1:
				header.text = "8 Марта";
				break;
			case 2:
				header.text = "Арамиль";
				break;
			case 3:
				header.text = "Ургупс";
				break;
			case 4:
				header.text = "Академ";
				break;
			case 5:
				header.text = "Сборная \n 05-06";
				break;
			case 6:
				header.text = "Сборная \n 2011";
				break;
			case 7:
				header.text = "Поляна";
				break;
			case 8:
				header.text = "Академ \n старшие";
				break;
			case 9:
				header.text = "8 Марта \n старшие";
				break;
			case 10:
				header.text = "Ургупс \n старшие";
				break;
			case 11:
				header.text = "Сборная \n 12-13";
				break;
			case 12:
				header.text = "Сакко \n старшие";
				break;
			case 13:
				header.text = "Поляна \n старшие";
				break;
			case 14:
				header.text = "Академ \n подгот";
				break;
			case 15:
				header.text = "8 Марта \n средние";
				break;
			case 16:
				header.text = "Ургупс \n 2";
				break;
			case 17:
				header.text = "Ургупс \n 3";
				break;
		}

	}
}	


